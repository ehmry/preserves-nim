# The Preserves Data Language

[Preserves](https://preserves.gitlab.io/preserves/) is a data model and serialization format. The Preserves model features the familiar boolean, integer, float, string, sequence, dictionaries, and set types. Less familiar to Nim are the symbol and record types. The symbol is a string that is elevated for use in type comparisons and the record is a labelled (usually by symbol) collection of fields.

The textual representation isn't necessary to study before using Preserves because the Preserves model is a subset of Nim types and a code-generator is available to convert Preserves schemas to Nim modules.

If you don't know why you need Preserves, see the [Syndicate library](https://git.syndicate-lang.org/ehmry/syndicate-nim).

## Preserves schema

[Here](https://git.syndicate-lang.org/syndicate-lang/syndicate-protocols/src/commit/ca92d99c524d99b6d3be04a0ba5383ec5a65b550/schemas/simpleChatProtocol.prs) is an example schema for a chat protocol:

```
; Present is a record with symbol "Present" as record label
; and one string field referred to as "username".
Present = <Present @username string>.

; Says is a record with symbol "Says" as record label
; and two fields referred to as "who" and "what".
Says = <Says @who string @what string>.
```

### Code Generation

The [preserves-schema-nim](./src/preserves/preserves_schema_nim.nim) utility would generate the following module for the preceding schema:
``` nim
type
  Says* {.preservesRecord: "Says".} = object
    `who`*: string
    `what`*: string

  Present* {.preservesRecord: "Present".} = object
    `username`*: string
```

There are two types corresponding to the two records defined in the schema. The `preservesRecord` pragma allows for a lossless conversion between the Nim type system and Preserves records.

``` nim
var
  present = Present(username: "Judy")
  pr = present.toPreserve()
assert $pr == """<Present "Judy">"""
assert present.fromPreserve(pr) == true
```

## Library

To parse or produce Preserves one should write a [schema](https://preserves.dev/preserves-schema.html) and generate a Nim module using the [preserves-schema-nim](./src/preserves/preserves_schema_nim.nim) utility. This module will contain Nim types corresponding to schema definitions. The `toPreserve` and`fromPreserve` routines will convert Nim types to and from Preserves. The `decodePreserves`, `parsePreserves`, `encode`, and `$` routines will convert `Preserve` objects to and from binary and textual encoding.

To debug the `toPreserves` and `fromPreserves` routines compile with `-d:tracePreserves`.

## Utilities
* preserves-schema-nim
* preserves-encode
* preserves-decode
* preserves-from-json
* preserves-to-json
* preserves-from-xml
* preserves-to-xml

### Installation
`preserves_encode` is a multi-call binary that implements `preserves-encode`, `preserves-decode`, `preserves-from-json`, and `preserves-to-json`, so the appropriate symlinks should be created during packaging.
