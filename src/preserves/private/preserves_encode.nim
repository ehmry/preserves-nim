# SPDX-FileCopyrightText: 2021 ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import std/[json, options, streams, xmlparser, xmltree]
from std/os import extractFilename, paramStr

import ../../preserves, ../jsonhooks, ../xmlhooks

when isMainModule:
  let command = extractFilename(paramStr 0)
  try:
    case command:
    of "preserves-encode":
      let pr = stdin.readAll.parsePreserves
      stdout.newFileStream.write(pr)
    of "preserves-decode":
      let pr = stdin.readAll.decodePreserves
      stdout.writeLine($pr)
    of "preserves-from-json":
      let
        js = stdin.newFileStream.parseJson
        pr = js.toPreserves
      stdout.newFileStream.write(pr)
    of "preserves-from-xml":
      let
        xn = stdin.newFileStream.parseXml
        pr = xn.toPreservesHook()
      stdout.newFileStream.write(pr)
    of "preserves-to-json":
      let
        pr = stdin.readAll.decodePreserves
        js = preservesTo(pr, JsonNode)
      if js.isSome:
        stdout.writeLine(get js)
      else:
        quit("Preserves not convertable to JSON")
    of "preserves-to-xml":
      let pr = stdin.readAll.decodePreserves
      var xn: XmlNode
      if fromPreserves(xn, pr):
        stdout.writeLine(xn)
      else:
        quit("Preserves not convertable to XML")
    else:
      quit("no behavior defined for " & command)
  except CatchableError:
    let msg = getCurrentExceptionMsg()
    quit(msg)
