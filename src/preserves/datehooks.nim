# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import std/times
import ../preserves

const
  label = "rfc3339"
  fullDateFormat = "yyyy-MM-dd"
  partialTimeFormat = "HH:mm:ss"
  fullTimeFormat = "HH:mm:sszzz"
  dateTimeFormat = "yyyy-MM-dd'T'HH:mm:sszzz"

proc toPreservesHook*(dt: DateTime): Value =
  initRecord("rfc3339", toPreserves($dt))

proc fromPreservesHook*(dt: var DateTime; pr: Value): bool =
  result = pr.isRecord(label, 1) and pr.record[0].isString
  if result:
    try:
      let
        s = pr.record[0].string
        n = len(s)
      if n == len(fullDateFormat):
        dt = parse(s, fullDateFormat)
      elif n == len(partialTimeFormat):
        dt = parse(s, partialTimeFormat)
      elif len(partialTimeFormat) < n and n <= len(fullTimeFormat):
        dt = parse(s, fullTimeFormat)
      elif len(fullTimeFormat) < n:
        dt = parse(s, dateTimeFormat)
      else:
        result = false
    except ValueError:
      result = false

runnableExamples:
  import std/[times, unittest]
  import preserves
  var a, b: DateTime
  a = now()
  var pr = a.toPreservesHook()
  check b.fromPreservesHook(pr)
  check $a == $b
