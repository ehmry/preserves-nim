# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  npeg,
  ../preserves, ./pegs

type
  Frame = tuple[value: Value, pos: int]
  Stack = seq[Frame]

proc shrink(stack: var Stack; n: int) = stack.setLen(stack.len - n)

template pushStack(v: Value) = stack.add((v, capture[0].si))

template collectEntries(result: var seq[Value]; stack: var Stack) =
  for frame in stack.mitems:
    if frame.pos > capture[0].si:
      result.add frame.value.move
  stack.shrink result.len

proc parseExpressions*(text: string): seq[Value] =
  let parser = peg("Document", stack: Stack):

    ws <- *{ ' ', '\t', '\r', '\n' }

    Document <- *Expr * ws * !1

    Annotation <-
      ('@' * SimpleExpr) |
      ('#' * {'\x20', '\x09', '\x21'} * @{'\r','\n'})

    Trailer <- *(ws * Annotation)

    Expr <- ws * (Punct | SimpleExpr) * Trailer

    Punct <- {',', ';'} | +':':
      pushStack initRecord("p", toSymbol $0)

    SimpleExpr <-
        Atom |
        Compound |
        Embedded |
        Annotated

    Embedded <- "#:" * SimpleExpr:
      pushstack stack.pop.value.embed

    Annotated <- Annotation * SimpleExpr

    Compound <- Sequence | Record | Block | Group | Set

    Sequence <-  '[' * *Expr * ws * ']':
      var pr = Value(kind: pkSequence)
      collectEntries(pr.sequence, stack)
      pushStack pr

    Record <- '<' * *Expr * ws * '>':
      var pr = Value(kind: pkRecord)
      collectEntries(pr.record, stack)
      pr.record.add toSymbol"r"
      pushStack pr

    Block <- '{' * *Expr * ws * '}':
      var pr = Value(kind: pkRecord)
      collectEntries(pr.record, stack)
      pr.record.add toSymbol"b"
      pushStack pr

    Group <- '(' * *Expr * ws * ')':
      var pr = Value(kind: pkRecord)
      collectEntries(pr.record, stack)
      pr.record.add toSymbol"g"
      pushStack pr

    Set <- "#{" * *Expr * ws * '}':
      var pr = Value(kind: pkRecord)
      collectEntries(pr.record, stack)
      pr.record.add toSymbol"s"
      pushStack pr

    Atom <- Preserves.Atom:
      pushStack parsePreserves($0)

  var stack: Stack
  let match = parser.match(text, stack)
  if not match.ok:
    raise newException(ValueError, "failed to parse Preserves Expressions:\n" & text[match.matchMax..text.high])

  result.setLen stack.len
  for i, _ in result:
    result[i] = move stack[i].value
