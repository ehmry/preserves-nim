# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import ../preserves

proc `%`*(v: bool|SomeFloat|SomeInteger|string|seq[byte]|Symbol): Value = v.toPreserves
  # Preserve an atomic Nim value.
