
import
  ../preserves, std/tables

type
  Ref* {.preservesRecord: "ref".} = object
    `module`*: ModulePath
    `name`*: Symbol
  Binding* {.preservesRecord: "named".} = object
    `name`*: Symbol
    `pattern`*: SimplePattern
  CompoundPatternKind* {.pure.} = enum
    `rec`, `tuple`, `tuplePrefix`, `dict`
  CompoundPatternRec* {.preservesRecord: "rec".} = object
    `label`*: NamedPattern
    `fields`*: NamedPattern
  CompoundPatternTuple* {.preservesRecord: "tuple".} = object
    `patterns`*: seq[NamedPattern]
  CompoundPatternTuplePrefix* {.preservesRecord: "tuplePrefix".} = object
    `fixed`*: seq[NamedPattern]
    `variable`*: NamedSimplePattern
  CompoundPatternDict* {.preservesRecord: "dict".} = object
    `entries`*: DictionaryEntries
  `CompoundPattern`* {.acyclic, preservesOr.} = ref object
    case orKind*: CompoundPatternKind
    of CompoundPatternKind.`rec`:
      `rec`*: CompoundPatternRec
    of CompoundPatternKind.`tuple`:
      `tuple`*: CompoundPatternTuple
    of CompoundPatternKind.`tuplePrefix`:
      `tupleprefix`*: CompoundPatternTuplePrefix
    of CompoundPatternKind.`dict`:
      `dict`*: CompoundPatternDict
  Definitions* = Table[Symbol, Definition]
  Bundle* {.preservesRecord: "bundle".} = object
    `modules`*: Modules
  DictionaryEntries* = OrderedTable[Value, NamedSimplePattern]
  SimplePatternKind* {.pure.} = enum
    `any`, `atom`, `embedded`, `lit`, `seqof`, `setof`, `dictof`, `Ref`
  SimplePatternAtom* {.preservesRecord: "atom".} = object
    `atomKind`*: AtomKind
  SimplePatternEmbedded* {.preservesRecord: "embedded".} = object
    `interface`*: SimplePattern
  SimplePatternLit* {.preservesRecord: "lit".} = object
    `value`*: Value
  SimplePatternSeqof* {.preservesRecord: "seqof".} = object
    `pattern`*: SimplePattern
  SimplePatternSetof* {.preservesRecord: "setof".} = object
    `pattern`*: SimplePattern
  SimplePatternDictof* {.preservesRecord: "dictof".} = object
    `key`*: SimplePattern
    `value`*: SimplePattern
  `SimplePattern`* {.acyclic, preservesOr.} = ref object
    case orKind*: SimplePatternKind
    of SimplePatternKind.`any`:
      `any`* {.preservesLiteral: "any".}: bool
    of SimplePatternKind.`atom`:
      `atom`*: SimplePatternAtom
    of SimplePatternKind.`embedded`:
      `embedded`*: SimplePatternEmbedded
    of SimplePatternKind.`lit`:
      `lit`*: SimplePatternLit
    of SimplePatternKind.`seqof`:
      `seqof`*: SimplePatternSeqof
    of SimplePatternKind.`setof`:
      `setof`*: SimplePatternSetof
    of SimplePatternKind.`dictof`:
      `dictof`*: SimplePatternDictof
    of SimplePatternKind.`Ref`:
      `ref`*: Ref
  SchemaField0* {.preservesDictionary.} = object
    `definitions`*: Definitions
    `embeddedType`*: EmbeddedTypeName
    `version`* {.preservesLiteral: "1".}: tuple[]
  Schema* {.preservesRecord: "schema".} = object
    `field0`*: SchemaField0
  NamedPatternKind* {.pure.} = enum
    `named`, `anonymous`
  `NamedPattern`* {.acyclic, preservesOr.} = ref object
    case orKind*: NamedPatternKind
    of NamedPatternKind.`named`:
      `named`*: Binding
    of NamedPatternKind.`anonymous`:
      `anonymous`*: Pattern
  EmbeddedTypeNameKind* {.pure.} = enum
    `false`, `Ref`
  `EmbeddedTypeName`* {.preservesOr.} = object
    case orKind*: EmbeddedTypeNameKind
    of EmbeddedTypeNameKind.`false`:
      `false`* {.preservesLiteral: "#f".}: bool
    of EmbeddedTypeNameKind.`Ref`:
      `ref`*: Ref
  NamedAlternative* {.preservesTuple.} = object
    `variantLabel`*: string
    `pattern`*: Pattern
  PatternKind* {.pure.} = enum
    `SimplePattern`, `CompoundPattern`
  `Pattern`* {.acyclic, preservesOr.} = ref object
    case orKind*: PatternKind
    of PatternKind.`SimplePattern`:
      `simplepattern`*: SimplePattern
    of PatternKind.`CompoundPattern`:
      `compoundpattern`*: CompoundPattern
  ModulePath* = seq[Symbol]
  DefinitionKind* {.pure.} = enum
    `or`, `and`, `Pattern`
  DefinitionOrField0* {.preservesTuple.} = object
    `pattern0`*: NamedAlternative
    `pattern1`*: NamedAlternative
    `patternN`* {.preservesTupleTail.}: seq[NamedAlternative]
  DefinitionOr* {.preservesRecord: "or".} = object
    `field0`*: DefinitionOrField0
  DefinitionAndField0* {.preservesTuple.} = object
    `pattern0`*: NamedPattern
    `pattern1`*: NamedPattern
    `patternN`* {.preservesTupleTail.}: seq[NamedPattern]
  DefinitionAnd* {.preservesRecord: "and".} = object
    `field0`*: DefinitionAndField0
  `Definition`* {.preservesOr.} = object
    case orKind*: DefinitionKind
    of DefinitionKind.`or`:
      `or`*: DefinitionOr
    of DefinitionKind.`and`:
      `and`*: DefinitionAnd
    of DefinitionKind.`Pattern`:
      `pattern`*: Pattern
  Modules* = Table[ModulePath, Schema]
  `AtomKind`* {.preservesOr, pure.} = enum
    `Boolean`, `Double`, `SignedInteger`, `String`, `ByteString`, `Symbol`
  NamedSimplePatternKind* {.pure.} = enum
    `named`, `anonymous`
  `NamedSimplePattern`* {.preservesOr.} = object
    case orKind*: NamedSimplePatternKind
    of NamedSimplePatternKind.`named`:
      `named`*: Binding
    of NamedSimplePatternKind.`anonymous`:
      `anonymous`*: SimplePattern
proc `$`*(x: Ref | Binding | CompoundPattern | Definitions | Bundle |
    DictionaryEntries |
    SimplePattern |
    Schema |
    NamedPattern |
    EmbeddedTypeName |
    NamedAlternative |
    Pattern |
    ModulePath |
    Definition |
    Modules |
    NamedSimplePattern): string =
  `$`(toPreserves(x))

proc encode*(x: Ref | Binding | CompoundPattern | Definitions | Bundle |
    DictionaryEntries |
    SimplePattern |
    Schema |
    NamedPattern |
    EmbeddedTypeName |
    NamedAlternative |
    Pattern |
    ModulePath |
    Definition |
    Modules |
    NamedSimplePattern): seq[byte] =
  encode(toPreserves(x))
