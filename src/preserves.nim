# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

{.push raises: [].}

import
  std/[assertions, options, sets, sequtils, strutils, tables, typetraits],
  pkg/bigints,
  ./preserves/private/[buffering, encoding, decoding, dot, macros, parsing, texts, values]

from std/algorithm import sort
from std/json import escapeJson, escapeJsonUnquoted

export options
export buffering, encoding, decoding, parsing, texts, values

when defined(tracePreserves):
  when defined(posix):
    template trace(args: varargs[untyped]) = {.cast(noSideEffect).}: stderr.writeLine(args)
  else:
    template trace(args: varargs[untyped]) = {.cast(noSideEffect).}: echo(args)
else:
  template trace(args: varargs[untyped]) = discard

func sortDict(pr: var Value) =
  sort(pr.dict) do (x, y: DictEntry) -> int:
    cmp(x.key, y.key)

func cannonicalize*(pr: var Value) =
  ## Cannonicalize a compound Preserves value by total ordering.
  case pr.kind
  of pkSequence:
    apply(pr.sequence, cannonicalize)
  of pkSet:
    apply(pr.set, cannonicalize)
    sort(pr.set)
  of pkDictionary:
    apply(pr.dict) do (e: var DictEntry):
      cannonicalize(e.val)
    sortDict(pr)
  else:
    discard

func toInt*(pr: Value): Option[int] =
  case pr.kind
  of pkRegister: result = some pr.register
  of pkBigInt: result = toInt[int](pr.bigint)
  else: discard

func isAtomic*(pr: Value): bool = pr.kind in atomKinds

func isBoolean*(pr: Value): bool {.inline.} = pr.kind == pkBoolean
  ## Check if ``pr`` is a Preserves boolean.

func isBoolean*(pr: Value; b: bool): bool {.inline.} =
  ## Check if ``pr`` is a equivalent to `bool` `b`.
  pr.isBoolean and pr.bool == b

func isFalse*(pr: Value): bool {.inline.} =
  ## Check if ``pr`` is equivalent to the zero-initialized ``Preserve``.
  pr.kind == pkBoolean and pr.bool == false

func isFloat*(pr: Value): bool {.inline.} = pr.kind == pkFloat
  ## Check if ``pr`` is a Preserve float.

func isFloat*(pr: Value; f: float): bool {.inline.} =
  ## Check if ``pr`` is equivalent to `float` `f`.
  pr.isFloat and pr.float == f

func isDouble*(pr: Value): bool {.deprecated: "use isFloat".} = pr.kind == pkFloat

func isInteger*(pr: Value): bool {.inline.} =
  ## Check if ``pr`` is a Preserve integer.
  pr.kind == pkRegister or pr.kind == pkBigInt

func isInteger*(pr: Value; i: SomeInteger): bool {.inline.} =
  ## Check if ``pr`` is a Preserve integer equivalent to `i`.
  case pr.kind
  of pkRegister: pr.register == i.int
  of pkBigInt: pr.bigint == i.initBigInt
  else: false

func isString*(pr: Value): bool {.inline.} = pr.kind == pkString
  ## Check if ``pr`` is a Preserve text string.

func isString*(pr: Value; s: string): bool {.inline.} =
  ## Check if ``pr`` is a Preserve text string equivalent to `s`.
  pr.kind == pkString and pr.string == s

func isByteString*(pr: Value): bool {.inline.} = pr.kind == pkByteString
  ## Check if ``pr`` is a Preserves byte string.

func isByteString*(pr: Value; bytes: openarray[byte]): bool {.inline.} =
  ## Check if ``pr`` is a Preserves byte string.
  pr.isByteString and
    pr.bytes.len == bytes.len and
    equalMem(pr.bytes[0].addr, bytes[0].addr, bytes.len)

func isSymbol*(pr: Value): bool {.inline.} = pr.kind == pkSymbol
  ## Check if `pr` is a Preserves symbol.

func isSymbol*(pr: Value; sym: string|Symbol): bool {.inline.} =
  ## Check if ``pr`` is a Preserves symbol of ``sym``.
  (pr.kind == pkSymbol) and (pr.symbol == Symbol(sym))

func label*(pr: Value): Value {.inline.} =
  ## Return the label of record value.
  pr.record[pr.record.high]

func arity*(pr: Value): int {.inline.} =
  ## Return the number of fields in record `pr`.
  pred(pr.record.len)

func isRecord*(pr: Value): bool {.inline.} = (pr.kind == pkRecord) and (pr.record.len > 0)
  ## Check if ``pr`` is a Preserves record.

func isRecord*(pr: Value; label: string): bool {.inline.} =
  ## Check if ``pr`` is a Preserves record with the given label symbol.
  pr.kind == pkRecord and
    pr.record.len > 0 and
    pr.label.isSymbol(label)

func isRecord*(pr: Value; label: string; arity: Natural): bool {.inline.} =
  ## Check if ``pr`` is a Preserves record with the given label symbol and field arity.
  pr.kind == pkRecord and
    pr.record.len == succ(arity) and
    pr.label.isSymbol(label)

func isSequence*(pr: Value): bool {.inline.} = pr.kind == pkSequence
  ## Check if ``pr`` is a Preserves sequence.

func isTuple*(pr: Value; arity: Natural): bool {.inline.} =
  ## Check if ``pr`` is a Preserves sequence of length ``arity``.
  pr.kind == pkSequence and pr.sequence.len == arity

func isSet*(pr: Value): bool {.inline.} = pr.kind == pkSet
  ## Check if ``pr`` is a Preserves set.

func isDictionary*(pr: Value): bool {.inline.} = pr.kind == pkDictionary
  ## Check if ``pr`` is a Preserves dictionary.

func isEmbeddedLiteral*(pr: Value): bool {.inline.} =
  ## Check if ``pr`` is an embedded value.
  pr.embedded and pr.kind != pkEmbedded

func isEmbeddedRef*(pr: Value): bool {.inline.} =
  ## Check if ``pr`` is an embedded value.
  pr.kind == pkEmbedded

func isEmbedded*(pr: Value): bool {.inline.} =
  ## Check if ``pr`` is an embedded value.
  pr.isEmbeddedLiteral or pr.isEmbeddedRef

func `&`*(x, y: Value): Value {.raises: [ValueError].} =
  ## Concatenate operator.
  if x.kind != y.kind:
    raise newException(ValueError, "cannot concatenate heterogenous values")
  case x.kind
  of pkString:
    result = Value(kind: pkString, string: x.string & y.string)
  of pkByteString:
    result = Value(kind: pkByteString, bytes: x.bytes & y.bytes)
  of pkSequence:
    result = Value(kind: pkSequence, sequence: x.sequence & y.sequence)
  else:
    raise newException(ValueError, "cannot concatenate this value type")

func `&`*(x: Value; y: seq[Value]): Value {.raises: [ValueError].} =
  if x.kind != pkSequence:
    raise newException(ValueError, "cannot concatenate to non-sequence value")
  result = Value(kind: pkSequence, sequence: x.sequence & y)

func pop*(pr: var Value; key: Value; val: var Value): bool =
  ##  Deletes the `key` from a Preserves dictionary.
  ## Returns true, if the key existed, and sets `val` to the mapping
  ## of the key. Otherwise, returns false, and the `val` is unchanged.
  if pr.kind == pkDictionary:
    var i = 0
    while i < pr.dict.len:
      if pr.dict[i].key == key:
        val = move pr.dict[i].val
        delete(pr.dict, i .. i)
        return true
      inc i

func `[]`*(pr, key: Value): Value {.
    deprecated: "step using the `{}` operator", raises: [KeyError, ValueError].} =
  ## Select a value by `key` from `pr`.
  ## Works for sequences, records, and dictionaries.
  case pr.kind
  of pkDictionary:
    for (k, v) in pr.dict.items:
      if k == key: return v
    raise newException(KeyError, "value not in Preserves dictionary")
  of pkRecord, pkSequence:
    let idx = key.toInt
    if idx.isSome:
      result = pr[get idx]
    else:
      raise newException(ValueError, "invalid Preserves index")
  else:
    raise newException(ValueError, "invalid Preserves indexing")

func toSymbol*(s: sink string): Value {.inline.} =
  ## Create a Preserves symbol value.
  Value(kind: pkSymbol, symbol: Symbol s)

func initRecord*(label: Value; arity: Natural = 0): Value =
  ## Create a Preserves record value.
  result = Value(kind: pkRecord, record: newSeq[Value](arity.succ))
  result.record[arity] = label

func initRecord*(label: Value; args: varargs[Value, toPreserves]): Value =
  ## Create a Preserves record value.
  result = Value(kind: pkRecord,
      record: newSeqOfCap[Value](1+args.len))
  for arg in args:
    result.record.add(arg)
  result.record.add(label)

func initRecord*(label: string; args: varargs[Value, toPreserves]): Value {.inline.} =
  ## Create a Preserves record value.
  initRecord(toSymbol(label), args)

func toRecord*(items: varargs[Value, toPreserves]): Value =
  assert items.len > 0
  result = initRecord(items[0], items.len.pred)
  for i in 0..<items.high: result.record[i] = items[succ i]

func initSequence*(len: Natural = 0): Value =
  ## Create a Preserves sequence value.
  Value(kind: pkSequence, sequence: newSeq[Value](len))

func initSequenceOfCap*(cap: Natural): Value =
  ## Create a Preserves sequence value.
  Value(kind: pkSequence, sequence: newSeqOfCap[Value](cap))

func toSequence*(elems: varargs[Value, toPreserves]): Value =
  result = initSequenceOfCap(elems.len)
  for i, e in elems: result.sequence.add e

func initSet*(): Value = Value(kind: pkSet)
  ## Create a Preserves set value.

func toSet*(elems: varargs[Value, toPreserves]): Value =
  result = Value(kind: pkSet, set: newSeqOfCap[Value](elems.len))
  for e in elems: result.incl e

func initDictionary*(): Value = Value(kind: pkDictionary)
  ## Create a Preserves dictionary value.

func initDictionary*(cap: int): Value =
  ## Create a Preserves dictionary value.
  Value(kind: pkDictionary, dict: newSeqOfCap[DictEntry](cap))

func toDictionary*(pairs: openArray[(string, Value)]): Value =
  ## Create a Preserves dictionary value.
  result = Value(kind: pkDictionary)
  for (key, val) in pairs: result[toSymbol(key)] = val
  sortDict(result)

func toDictionary*(pairs: openArray[(Value, Value)]): Value =
  ## Create a Preserves dictionary value.
  result = Value(kind: pkDictionary)
  for (key, val) in pairs: result[key] = val
  sortDict(result)

func embed*(pr: sink Value): Value =
  ## Mark a Preserves value as embedded.
  result = pr
  result.embedded = true

func embed*(e: sink EmbeddedRef): Value =
  ## Create a Preserves value that embeds ``e``.
  Value(kind: pkEmbedded, embeddedRef: e, embedded: true)

func len*(pr: Value): int =
  ## Return the shallow count of values in ``pr``, that is the number of
  ## fields in a record, items in a sequence, items in a set, or key-value pairs
  ## in a dictionary.
  case pr.kind
  of pkRecord: pr.record.len.pred
  of pkSequence: pr.sequence.len
  of pkSet: pr.set.len
  of pkDictionary: pr.dict.len
  else: 0

iterator items*(pr: Value): Value =
  ## Shallow iterator over `pr`, yield the fields in a record,
  ## the items of a sequence, the items of a set, or the pairs
  ## of a dictionary.
  case pr.kind
  of pkRecord:
    for i in 0..pr.record.high.pred:
      yield pr.record[i]
  of pkSequence:
    for e in pr.sequence.items: yield e
  of pkSet:
    for e in pr.set.items: yield e
  of pkDictionary:
    for (k, v) in pr.dict.items:
      yield k # key can be an arbitrary Preserve
      yield v
  else: discard

iterator pairs*(pr: Value): DictEntry =
  assert(pr.kind == pkDictionary, "not a dictionary")
  for i in 0..pr.dict.high: yield pr.dict[i]

func fields*(pr: Value): seq[Value] {.inline.} =
  ## Return the fields of a record value.
  pr.record[0..pr.record.high.pred]

iterator fields*(pr: Value): Value =
  ## Iterate the fields of a record value.
  for i in 0..<pr.record.high: yield pr.record[i]

func unembed*(pr: Value; E: typedesc): Option[E] =
  ## Unembed an `E` value from a `Value` value.
  if pr.kind == pkEmbedded and pr.embeddedRef of E:
    result = some(E pr.embeddedRef)

template preservesRecord*(label: string) {.pragma.}
  ## Serialize this object or tuple as a record.
  ## See ``toPreserve``.

func hasPreservesRecordPragma*(T: typedesc): bool =
  ## Test if a type has a `{.preservesRecord: "…".}` pragma attached.
  hasCustomPragma(T, preservesRecord)

func recordLabel*(T: typedesc): string =
  ## Get the record label set by a pragma on a type.
  runnableExamples:
    type Foo {.preservesRecord: "bar".} = object
      n: int
    assert recordLabel(Foo) == "bar"
  T.getCustomPragmaVal(preservesRecord)

func step(pr, idx: Value): Option[Value] =
  if pr.isDictionary:
    for (k, v) in pr.dict.items:
      if k == idx:
        result = some(v)
        break
  elif (pr.isRecord or pr.isSequence):
    var o = idx.toInt
    if o.isSome:
      var i = get o
      if i < pr.len:
        try: result = some(pr[i])
        except CatchableError: discard

func `{}`*(pr: Value; path: varargs[Value, toPreserves]): Option[Value] =
  ## Step into `pr` by index `idx`.
  ## Works for sequences, records, and dictionaries.
  runnableExamples:
    assert parsePreserves("<foo 1 2>"){1} == some(2.toPreserves)
    assert parsePreserves("{ foo: 1 bar: 2}"){Symbol"foo"}  == some(1.toPreserves)
    assert parsePreserves("[ ]"){1} == none(Value)
  result = some(pr)
  for index in path:
    if result.isSome:
      result = step(result.get, index)

func step*(pr: Value; path: varargs[Value, toPreserves]): Option[Value] {.
    deprecated: "step using the `{}` operator".} = `{}`(pr, path)

template preservesTuple*() {.pragma.}
  ## Serialize this object or tuple as a tuple.
  ## See ``toPreserves``.

template preservesTupleTail*() {.pragma.}
  ## Serialize this object field to the end of its containing tuple.
  ## See ``toPreserves``.

func hasPreservesTuplePragma*(T: typedesc): bool =
  ## Test if a type has a `preservesTuple` pragma attached.
  hasCustomPragma(T, preservesTuple)

template preservesDictionary*() {.pragma.}
  ## Serialize this object or tuple as a dictionary.
  ## See ``toPreserves``.

func hasPreservesDictionaryPragma*(T: typedesc): bool =
  ## Test if a type has a `preservesDictionary` pragma attached.
  hasCustomPragma(T, preservesDictionary)

template preservesOr*() {.pragma.}
  ## Serialize this object as an ``or`` alternative.
  ## See ``toPreserves``.

template preservesLiteral*(value: typed) {.pragma.}
  ## Serialize a Preserves literal within this object.
  ## See ``toPreserves``.

template preservesEmbedded*() {.pragma.}
  ## Pragma to mark a type or value as embedded.

template unpreservable*() {.pragma.}
  ## Pragma to forbid a type from being converted by ``toPreserves``.
  ## Useful for preventing an embeded type from being encoded
  ## as its native type.
  ## Unpreservability is asserted at runtime.

func toPreserves*(x: bool): Value = Value(kind: pkBoolean, bool: x)
func toPreserves*(x: SomeFloat): Value = Value(kind: pkFloat, float: float(x))
func toPreserves*(x: int): Value = Value(kind: pkRegister, register: x)
func toPreserves*(x: string): Value = Value(kind: pkString, string: x)
func toPreserves*(x: seq[byte]): Value = Value(kind: pkByteString, bytes: x)
func toPreserves*(x: Symbol): Value = Value(kind: pkSymbol, symbol: x)
func toPreserves*(x: enum): Value = toSymbol($x)

func toPreserves*[T](x: T): Value {.gcsafe.} =
  ## Serializes ``x`` to Preserves. Can be customized by defining
  ## ``toPreservesHook(x: T)`` in the calling scope.
  ## Any ``toPreservesHook`` that does not compile will be discarded;
  ## *Write tests for your hooks!*
  ##
  ## When `tracePreserves` is defined (`-d:tracePreserves`) a diagnostic
  ## trace is printing during `toPreserve`.
  when (T is Value): result = x
  elif T is Value:
    result = cast[Value](x)
  elif compiles(toPreservesHook(x)):
    try: result = toPreservesHook(x)
    except Exception as err:
      raiseAssert err.msg
        # Escalate exceptions to defects.
    cannonicalize(result)
  elif T is array | seq:
    result = Value(kind: pkSequence, sequence: newSeqOfCap[Value](x.len))
    for v in x.items: result.sequence.add(toPreserves(v))
  elif T is set:
    result = Value(kind: pkSet, set: newSeqOfCap[Value](x.len))
    for v in x.items: result.incl(toPreserves(v))
    cannonicalize(result)
  elif T is tuple:
    result = Value(kind: pkSequence,
      sequence: newSeqOfCap[Value](tupleLen(T)))
    for xf in fields(x):
      result.sequence.add(toPreserves(xf))
  elif T is Ordinal:
    result = Value(kind: pkRegister, register: x.ord)
    assert result.register.T == x
  elif T is EmbeddedRef:
    result = embed(x)
  elif T is ptr | ref:
    when T.hasCustomPragma(preservesEmbedded):
      result = embed(x)
    else:
      if system.`==`(x, nil): result = initRecord("null")
      else: result = toPreserves(x[])
  elif T is SomeInteger:
    result = Value(kind: pkRegister, register: x.int)
    assert result.register.T == x
  elif T is distinct:
    result = toPreserves(x.distinctBase)
  elif T is object:
    template fieldToPreserve[F](key: string; field: F): Value {.used.} =
      when x.dot(key).hasCustomPragma(preservesLiteral):
        const atom = x.dot(key).getCustomPragmaVal(preservesLiteral).parsePreservesAtom
        when x.dot(key).hasCustomPragma(preservesEmbedded):
          atom.toPreservesHook().embed
        else:
          atom.toPreservesHook()
      elif F is EmbeddedRef:
        embed(field)
      elif x.dot(key).hasCustomPragma(preservesEmbedded):
        var pf = toPreserves(field)
        pf.embedded = true
        pf
      else:
        field.toPreserves
          # checking for the embedded pragma here yields false positives
    when T.hasCustomPragma(unpreservable):
      raiseAssert($T & " is unpreservable")
    elif T.hasCustomPragma(preservesOr):
      var hasKind, hasVariant: bool
      for k, v in x.fieldPairs:
        if k == "orKind":
          assert(not hasKind)
          hasKind = true
        else:
          assert(hasKind and not hasVariant)
          result = fieldToPreserve(k, v)
          hasVariant = true
    elif T.hasCustomPragma(preservesRecord):
      result = Value(kind: pkRecord)
      for label, field in x.fieldPairs:
        when x.dot(label).hasCustomPragma(preservesTupleTail):
          for e in field.items:
            result.record.add(toPreserves(e))
        else:
          var pr = fieldToPreserve(label, field)
          result.record.add(pr)
      result.record.add(toSymbol(T.getCustomPragmaVal(preservesRecord)))
    elif T.hasCustomPragma(preservesTuple):
      result = initSequence(0)
      for label, field in x.fieldPairs:
        when x.dot(label).hasCustomPragma(preservesTupleTail):
          for y in field.items:
            result.sequence.add(toPreserves(y))
          # TODO: what if there are fields after the tail?
        else:
          var pr = fieldToPreserve(label, field)
          result.sequence.add(pr)
    elif T.hasCustomPragma(preservesDictionary):
      result = initDictionary()
      for key, val in x.fieldPairs:
        when val is Option:
          if val.isSome:
            var pr = fieldToPreserve(key, val.get)
            if x.dot(key).hasCustomPragma(preservesEmbedded):
              pr.embedded = true
            result[key.toSymbol] = pr
        else:
          var pr = fieldToPreserve(key, val)
          if x.dot(key).hasCustomPragma(preservesEmbedded):
            pr.embedded = true
          result[key.toSymbol] = pr
      sortDict(result)
    else:
      {.warning: "failed to preserve object " & $T .}
      result = toPreservesHook(x)
  else:
    {.warning: "failed to preserve " & $T .}
    result = toPreservesHook(x)
    # the hook doesn't compile but produces a useful error
  trace T, " -> ", result

func toPreservesHook*(a: Atom): Value =
  result = Value(kind: a.kind)
  case a.kind
  of pkBoolean:
    result.bool = a.bool
  of pkFloat:
    result.float = a.float
  of pkRegister:
    result.register = a.register
  of pkBigInt:
    result.bigint = a.bigint
  of pkString:
    result.string = a.string
  of pkByteString:
    result.bytes = a.bytes
  of pkSymbol:
    result.symbol = a.symbol
  else:
    discard

func toPreservesHook*[N](bytes: array[N, byte]): Value =
  Value(kind: pkByteString, bytes: bytes.toSeq)

func toPreservesHook*[T](set: HashSet[T]): Value {.raises: [Exception].} =
  ## Hook for preserving ``HashSet``.
  result = Value(kind: pkSet, set: newSeqOfCap[Value](set.len))
  for e in set: result.incl toPreserves(e)
  cannonicalize(result)

func toPreservesHook*[A, B](table: Table[A, B]|TableRef[A, B]|OrderedTable[A, B]|OrderedTableRef[A, B]): Value =
  ## Hook for preserving ``Table``.
  result = initDictionary()
  for k, v in table.pairs:
    result[toPreserves(k)] = toPreserves(v)
  sortDict(result)

func toPreservesHook*(o: Option): Value =
  o.get.toPreserves

func fromAtom*[T](v: var T; a: ATom): bool =
  if T is Atom:
    v = a
    result = true
  if T is Value:
    v = a.toPreservesHook
    result = true
  elif T is enum:
    if a.kind == pkSymbol:
      try:
        v = parseEnum[T](string a.symbol)
        result = true
      except ValueError: discard
  elif T is bool:
    if a.kind == pkBoolean:
      v = a.bool
      result = true
  elif T is SomeInteger:
    if a.kind == pkRegister:
      result = a.register.T < high(T)
      if result:
        v = T a.register
    elif a.kind == pkBigInt:
      var o = toInt[T](a.bigint)
      result = o.isSome
      if result: v = o.get
  elif T is seq[byte]:
    if a.kind == pkByteString:
      v = a.bytes
      result = true
  elif T is SomeFloat:
    case a.kind
    of pkFloat:
      v = T a.float
      result = true
    else: discard
  elif T is Ordinal | SomeInteger:
    if a.kind == pkRegister:
      v = T(a.register)
      result = int(v) == a.register
    elif a.kind == pkBigInt:
      var o = toInt[T](a.bigint)
      if o.isSome:
        v = get o
        result = true
  elif T is string:
    if a.kind == pkString:
      v = a.string
      result = true
  elif T is Symbol:
    if a.kind == pkSymbol:
      v = a.symbol
      result = true
  elif T is distinct:
    result = fromAtom(v.distinctBase, a)

proc fromPreserves*[T](v: var T; pr: Value): bool {.raises: [Exception].} =
  ## Inplace version of `preservesTo`. Returns ``true`` on
  ## a complete match, otherwise returns ``false``.
  ## Can be customized with `fromPreservesHook(x: T; var pr: Value): bool`.
  ## Any ``fromPreservesHook`` that does not compile will be discarded;
  ## *Write tests to check hooks directly because a ``fromPreservesHook``
  ## that does not compile in the scope of ``fromPreserves`` will be
  ## silently rejected.*
  ## Please do not raise exceptions from hooks.
  ##
  ## When `tracePreserves` is defined (`-d:tracePreserves`) a diagnostic
  ## trace is printing during `fromPreserve`.
  runnableExamples:
    type Foo {.preservesRecord: "foo".} = object
      x, y: int
    var foo: Foo
    assert(fromPreserves(foo, parsePreserves("""<foo 1 2>""")))
    assert(foo.x == 1)
    assert(foo.y == 2)
  try: # Cannot eliminate exceptions from indexing and hooks.
    when T is Value:
      v = pr
      result = true
    elif T is Value:
      v = pr
      result = true
    elif compiles(fromPreservesHook(v, pr)):
      result = fromPreservesHook(v, pr)
    elif T is enum:
      if pr.isSymbol:
        try:
          v = parseEnum[T](string pr.symbol)
          result = true
        except ValueError: discard
    elif T is bool:
      if pr.kind == pkBoolean:
        v = pr.bool
        result = true
    elif T is SomeInteger:
      if pr.kind == pkRegister:
        v = T(pr.register)
        result = true
    elif T is seq[byte]:
      if pr.kind == pkByteString:
        v = pr.bytes
        result = true
    elif T is seq:
      if pr.kind == pkSequence:
        v.setLen(pr.len)
        result = true
        for i, e in pr.sequence:
          result = result and fromPreserves(v[i], pr.sequence[i])
          if not result:
            v.setLen 0
            break
    elif T is array:
      if pr.kind == pkSequence and pr.len == v.len:
        result = true
        for i, e in pr.sequence:
          if not fromPreserves(v[i], pr.sequence[i]):
            return false
    elif T is SomeFloat:
      if pr.kind == pkFloat:
        v = (T)pr.float
        result = true
    elif T is Ordinal | SomeInteger:
      case pr.kind
      of pkRegister:
        v = (T)pr.register
        result = true
      of pkBigInt:
         var o = toInt[T](pr.bigint)
         result = o.isSome
         if result: v = get o
      else: discard
    elif T is string:
      if pr.kind == pkString:
        v = pr.string
        result = true
    elif T is Symbol:
      if pr.kind == pkSymbol:
        v = pr.symbol
        result = true
    elif T is distinct:
      result = fromPreserve(v.distinctBase, pr)
    elif T is tuple:
      case pr.kind
      of pkSequence:
        if pr.len <= tupleLen(T):
          result = true
          var i {.used.}: int
          for f in fields(v):
            if result and i < pr.len:
              try: result = result and fromPreserves(f, pr[i])
              except CatchableError: reset result
            inc i
      of pkDictionary:
        if tupleLen(T) <= pr.len:
          result = true
          for key, val in fieldPairs(v):
            let pv = pr{[key.toSymbol]}
            result =
              if pv.isSome: fromPreserves(val, get pv)
              else: false
            if not result: break
      else: discard
    elif T is ref:
      when T.hasCustomPragma(preservesEmbedded):
        if (pr.kind == pkEmbedded) and (pr.embeddedRef of T):
          v = T(pr.embeddedRef)
          result = true
      else:
        if isNil(v): new(v)
        result = fromPreserves(v[], pr)
    elif T is object:
      template fieldFromPreserve[T](key: string; val: T; pr: Value): bool {.used.} =
        when v.dot(key).hasCustomPragma(preservesLiteral):
          const atom = v.dot(key).getCustomPragmaVal(preservesLiteral).parsePreservesAtom
          pr == atom.toPreservesHook()
        elif v.dot(key).hasCustomPragma(preservesEmbedded):
          when T is EmbeddedRef:
            if pr.kind == pkEmbedded and pr.embeddedRef of T:
              val = T(pr.embeddedRef)
              true
            else: false
          else: fromPreserves(val, pr)
        else: fromPreserves(val, pr)
      when T.hasCustomPragma(unpreservable):
        raiseAssert($T & " is unpreservable")
      elif T.hasCustomPragma(preservesRecord):
        if pr.isRecord and pr.label.isSymbol(T.getCustomPragmaVal(preservesRecord)):
          result = true
          var i: int
          for name, field in fieldPairs(v):
            when v.dot(name).hasCustomPragma(preservesTupleTail):
              v.dot(name).setLen(pr.record.len.pred - i)
              var j: int
              while result and i < pr.record.high:
                result = result and fromPreserves(v.dot(name)[j], pr.record[i])
                inc i
                inc j
              break
            else:
              if result and i <= pr.len:
                result = result and fieldFromPreserve(name, field, pr.record[i])
              inc i
          result = result and (i <= pr.len)
      elif T.hasCustomPragma(preservesTuple):
        if pr.isSequence:
          result = true
          var i: int
          for name, field in fieldPairs(v):
            when v.dot(name).hasCustomPragma(preservesTupleTail):
              if pr.len >= i:
                setLen(v.dot(name), pr.len - i)
                var j: int
                while result and i < pr.len:
                  result = result and fieldFromPreserve(name, v.dot(name)[j], pr.sequence[i])
                  inc i
                  inc j
            else:
              if result and i < pr.len:
                result = result and fieldFromPreserve(name, field, pr.sequence[i])
              inc i
          result = result and (i == pr.len)
      elif T.hasCustomPragma(preservesDictionary):
        if pr.isDictionary:
          result = true
          var i: int
          for key, field in fieldPairs(v):
            if not result: break
            let val = pr{[key.toSymbol]}
            when field is Option:
              if val.isSome:
                discard fieldFromPreserve(key, v.dot(key), val.get)
            else:
              inc i
              result = result and val.isSome
              if result:
                var pr = val.get
                result = result and fieldFromPreserve(key, v.dot(key), pr)
          result = result and (i <= pr.len)
      elif T.hasCustomPragma(preservesOr):
        for kind in typeof(T.orKind):
          v = T(orKind: kind)
          var fieldWasFound = false
          for key, val in fieldPairs(v):
            when key != "orKind": # fieldPairs unwraps early
              result = fieldFromPreserve(key, v.dot(key), pr)
              fieldWasFound = true
              break
          if not fieldWasFound:
            # hopefully a `discard` of-branch, so discard `pr`
            result = true
          if result: break
      else:
        if pr.isDictionary:
          result = true
          var i: int
          for key, _ in fieldPairs(v):
            if not result: break
            let val = pr{[key.toSymbol]}
            result = result and val.isSome
            if result:
              result = result and fieldFromPreserve(key, v.dot(key), val.get)
            inc i
          result = result and (i <= pr.len)
    else:
      result = fromPreservesHook(v, pr)
        # a previous branch determined that the hook does not compile but
        # calling it here explicitly produces a reasonable compiler error
  except CatchableError: reset result
  when defined(tracePreserves):
    if not result:
      trace T, " !- ", pr
    else:
      trace T, " <- ", pr

func fromPreserves*[T](v: var T; pr: Option[Value]): bool =
  if pr.isSome: result = fromPreserves(v, get(pr))

proc preservesTo*(pr: Value; T: typedesc): Option[T] {.raises: [Exception].} =
  ## Reverse of `toPreserve`.
  runnableExamples:
    type Foo {.preservesRecord: "foo".} = object
      x, y: int

    assert(parsePreserves("""<foo "abc">""").preservesTo(Foo).isNone)
    assert(parsePreserves("""<bar 1 2>""").preservesTo(Foo).isNone)
    assert(parsePreserves("""<foo 1 2>""").preservesTo(Foo).isSome)
  var v: T
  if fromPreserves(v, pr):
    result = some(move v)

func preservesTo*(opr: Option[Value]; T: typedesc): Option[T] =
  try: result = opr.flatMap(proc (pr: Value): Option[T] = preservesTo(pr, T))
  except: discard
    # TODO: better effects tracking

func fromPreservesHook*[T](v: var set[T]; pr: Value): bool =
  ## Hook for unpreserving a `set`.
  if pr.kind == pkSet:
    reset v
    result = true
    var vv: T
    for e in pr.set:
      result = fromPreserves(vv, e)
      if result:
        v.incl vv
      else:
        reset v
        break

func fromPreservesHook*[T](set: var HashSet[T]; pr: Value): bool {.raises: [Exception].} =
  ## Hook for preserving ``HashSet``.
  if pr.kind == pkSet:
    result = true
    set.init(pr.set.len)
    var e: T
    for pe in pr.set:
      result = fromPreserves(e, pe)
      if not result: break
      set.incl(move e)

proc fromPreservesHook*[A,B](t: var (Table[A,B]|TableRef[A,B]|OrderedTable[A, B]|OrderedTableRef[A, B]); pr: Value): bool {.raises: [Exception].} =
  if pr.isDictionary:
    try:
      when t is TableRef[A,B]:
        if t.isNil: new t
      result = true
      var a: A
      var b: B
      for (k, v) in pr.dict.items:
        result = fromPreserves(a, k) and fromPreserves(b, v)
        if not result:
          clear t
          break
        t[move a] = move b
      when t is OrderedTable[A, B] or t is OrderedTableRef[A, B]:
        if result:
          sort(t) do (x, y: (A, B)) -> int: cmp(x[0], y[0])
    except CatchableError: reset result

func fromPreservesHook*[T](opt: var Option[T]; pr: Value): bool {.raises: [Exception].} =
  when T is ref:
    opt = some(new T)
  else:
    opt = some(default T)
  result = opt.get.fromPreserves(pr)
  if not result:
    opt = none(T)

when isMainModule:
  block:
    var t: Table[int, string]
    var pr = t.toPreservesHook()
    assert fromPreservesHook(t, pr)

  block:
    var t: HashSet[string]
    var pr = t.toPreservesHook()
    assert fromPreservesHook(t, pr)

func sort*[T](table: var OrderedTable[Value, T]) =
  ## Sort `OrderedTable` keys by Preserves total ordering.
  table.sort do (x, y: (Value, T)) -> int: cmp(x[0], y[0])

proc apply*(result: var Value; op: proc(_: var Value)) {.effectsOf: op.} =
  proc recurse(result: var Value) = apply(result, op)
  op(result)
  case result.kind
  of pkBoolean, pkFloat, pkRegister, pkBigInt,
      pkString, pkByteString, pkSymbol, pkEmbedded:
    discard
  of pkRecord:
    apply(result.record, recurse)
  of pkSequence:
    apply(result.sequence, recurse)
  of pkSet:
    apply(result.set, recurse)
  of pkDictionary:
    apply(result.dict) do (e: var DictEntry):
      recurse(e.key)
      recurse(e.val)
  cannonicalize(result)

func getOrDefault*[T, V](pr: Value; key: string; default: V): V =
  ## Retrieves the value of `pr[key]` if `pr` is a dictionary containing `key`
  ## or returns the `default` value.
  var sym = toSymbol(key, T)
  if pr.kind == pkDictionary:
    for (k, v) in pr.dict:
      if sym == k:
        if fromPreserves(result, v): return
        else: break
  default

func keysToSymbols*(pr: var Value) =
  ## Replace all string keys in a dictionary with symbols.
  if pr.kind == pkDictionary:
    var i = 0
    while i < pr.dict.len:
      if pr.dict[i][0].isString:
        pr.dict[i][0] = pr.dict[i][0].string.toSymbol
      i.inc()

{.pop.}

proc mapEmbeds*(pr: sink Value; op: proc (x: Value): Value): Value {.effectsOf: op.} =
  ## Process all embeds in a `Value`.
  case pr.kind
  of pkBoolean, pkFloat, pkRegister, pkBigInt,
      pkString, pkByteString, pkSymbol, pkEmbedded:
    result = pr
  of pkRecord:
    result = Value(kind: pr.kind)
    result.record = map(pr.record) do (x: Value) -> Value:
      mapEmbeds(x, op)
  of pkSequence:
    result = Value(kind: pr.kind)
    result.sequence = map(pr.sequence) do (x: Value) -> Value:
      mapEmbeds(x, op)
  of pkSet:
    result = Value(kind: pr.kind)
    result.set = map(pr.set) do (x: Value) -> Value:
      mapEmbeds(x, op)
  of pkDictionary:
    result = Value(kind: pr.kind)
    result.dict = map(pr.dict) do (e: DictEntry) -> DictEntry:
      (mapEmbeds(e.key, op), mapEmbeds(e.val, op))
  if pr.embedded or pr.kind == pkEmbedded:
    result = op(result)
  cannonicalize(result)
    # TODO: is this necessary?
