# SPDX-FileCopyrightText: 2021 ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/unittest,
  preserves

suite "BufferedDecoder":

  test "half-string":
    var
      buf = newBufferedDecoder()
      pr = Value(kind: pkByteString, bytes: newSeq[byte](23))
    for i, _ in pr.bytes:
      pr.bytes[i] = byte(i)
    let bin = encode(pr)
    for i in 0..32:
      checkpoint $i
      let j = (i+2) and 0xf
      feed(buf, bin[0..<j])
      feed(buf, bin[j..bin.high])
      var v = decode(buf)
      check v.isSome
