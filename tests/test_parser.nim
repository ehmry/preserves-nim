# SPDX-FileCopyrightText: 2021 ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[streams, strutils, unittest],
  preserves

const examples = [
("""<capture <discard>>""", "\xB4\xB3\x07capture\xB4\xB3\x07discard\x84\x84"),
("""[1 2 3 4]""", "\xB5\xB0\x01\x01\xB0\x01\x02\xB0\x01\x03\xB0\x01\x04\x84"),
("""[-2 -1 0 1]""", "\xB5\xB0\x01\xFE\xB0\x01\xFF\xB0\x00\xB0\x01\x01\x84"),
(""""hello"""", "\xB1\x05hello"),
("""" \"hello\" """", "\xB1\x09 \"hello\" "),
("""["a" b #"c" [] #{} #t #f]""", "\xB5\xB1\x01a\xB3\x01b\xB2\x01c\xB5\x84\xB6\x84\x81\x80\x84"),
("""-257""", "\xB0\x02\xFE\xFF"),
("""-1""", "\xB0\x01\xFF"),
("""0""", "\xB0\x00"),
("""1""", "\xB0\x01\x01"),
("""255""", "\xB0\x02\x00\xFF"),
("""1.0""", "\x87\x08\x3F\xF0\x00\x00\x00\x00\x00\x00"),
("""-1.202e300""", "\x87\x08\xFE\x3C\xB7\xB7\x59\xBF\x04\x26"),
("""#=#x"B4B30763617074757265B4B307646973636172648484"""", "\xB4\xB3\x07capture\xB4\xB3\x07discard\x84\x84"),
("""#f""", "\x80")
]

suite "parse":
  for (txt, bin) in examples:
    test txt:
      checkpoint(txt)
      let test = parsePreserves(txt)
      checkpoint($test)
      block:
        let
          a = test
          b = decodePreserves(bin)
        check(a == b)
      block:
        let
          a = encode test
          b = bin
        check(cast[string](a).toHex == b.toHex)
      if test.isAtomic:
        discard parsePreservesAtom(txt)

suite "stream":
  let s = newStringStream("""A B C""")
  for i in 1..3:
    test $i:
      let v = s.parsePreserves
      checkpoint $v
      check v.isSome
  let v = s.parsePreserves
  check v.isNone
