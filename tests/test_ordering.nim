# SPDX-FileCopyrightText: 2021 ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[algorithm, random, tables],
  std/unittest,
  preserves,
  preserves/sugar

proc isUnordered(elems: openarray[Value]): bool =
  for i in 1..elems.high:
    if elems[pred i] > elems[i]:
      return true

suite "total-order":
  var values = [
      %true,
      %0,
      %"z",
      %"xyz",
      toRecord("x".toSymbol),
      toSequence(%"А", %"Б", %"В"),
      toset(%0, %1, %2),
      toDictionary({ %false: %0, %true: %1 }),
    ]

  shuffle values
  assert values.isUnordered

  var dict = initDictionary()
  for v in values:
    dict[v] = %false

  test "dictionary-order":
    var prevKey: Value
    for (key, _) in dict.pairs:
      check prevKey < key
      prevKey = key

  test "OrderedTable":
    var table = initOrderedTable[Value, bool]()
    check table.fromPreserves(dict)

    var prevKey: Value
    for key in table.keys:
      check prevKey < key
      prevKey = key
