# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/unittest,
  preserves

suite "step":
  var data = parsePreserves """
      <foo "bar" [ 0.0 {a: #f, "b": #t } ] >
    """

  var o = some data
  for i in [1.toPreserves, 1.toPreserves, "b".toPreserves]:
      o = o.get{i}
      check o.isSome
