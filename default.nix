{
  pkgs ? import <nixpkgs> { },
  src ? if pkgs.lib.inNixShell then null else pkgs.lib.cleanSource ./.,
}:
let
  buildNimSbom = pkgs.callPackage ./build-nim-sbom.nix { };
in
buildNimSbom (finalAttrs: {
  inherit src;
  nimFlags = [ "--path:src" ];
  postInstall = ''
    pushd $out/bin
    for link in preserves-decode \
        preserves-from-json preserves-to-json \
        preserves-from-xml preserves-to-xml
      do ln -s preserves-encode $link
    done
    popd
  '';
}) ./sbom.json
